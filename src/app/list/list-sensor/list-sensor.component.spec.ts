import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { ListSensorComponent } from './list-sensor.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {NgxPaginationModule} from 'ngx-pagination';
import {SensorService} from '../../services/sensor-service/sensor.service';
import {Sensor} from '../../models/sensor';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs/Observable';

describe('ListSensorComponent', () => {
  let component: ListSensorComponent;
  const stationId = 900;
  let fixture: ComponentFixture<ListSensorComponent>;
  let sensorService : SensorService;
  let sensor: Sensor;
  let activatedRoute: Object;

  beforeEach(async(() => {
    sensor = new Sensor();

    TestBed.configureTestingModule({
      declarations: [
        ListSensorComponent
      ],
      providers: [
        SensorService
      ],
      imports: [
        NgxPaginationModule,
        HttpClientTestingModule,
        RouterTestingModule
      ]
    }).compileComponents().then(() => {
      fixture = TestBed.createComponent(ListSensorComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

    sensorService = TestBed.get(SensorService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSensorComponent);
    component = fixture.componentInstance;
    activatedRoute = fixture.debugElement.injector.get(ActivatedRoute);
    activatedRoute["params"] = Observable.of({id: stationId});
    fixture.detectChanges();
  });

  afterEach(async(() => {
    component = null;
    fixture.destroy();
    sensorService = null;
    sensor = null;
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSensorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit()', () => {
    it('should get stations from stationService', fakeAsync(() => {
      //Arrange
      spyOn(sensorService, 'getSensors').and.returnValue(Observable.of([sensor]));
      tick();

      //Act
      component.ngOnInit();
      tick();

      //Assert
      expect(sensorService.getSensors).toHaveBeenCalledWith(stationId);
    }))
  });

  describe('pageChanged(page : number)', () => {
    it('should change the current page', fakeAsync(() => {
      //Arrange
      let oldPage = 1;
      component.currentPage = oldPage;
      let newPage = 2;

      //Act
      component.pageChanged(newPage);

      //Assert
      expect(component.currentPage).toBe(newPage);
    }))
  });
});
