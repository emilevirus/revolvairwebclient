import {Component, OnInit, NgZone} from '@angular/core';
import {divIcon, latLng, marker, tileLayer, point, Marker} from 'leaflet';
import {getMarkerSvg} from './marker-icon';
import {StationService} from '../services/station-service/station.service';
import {Router} from '@angular/router';

let OPENSTREETMAP_URL = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
let MAP_CENTER_QUEBEC = latLng(46.7841866, -71.2852583);

@Component({
  selector: 'app-station-map',
  templateUrl: './station-map.component.html',
  styleUrls: ['./station-map.component.css']
})
export class StationMapComponent implements OnInit {

    constructor(private stationService : StationService,
                private router : Router,
                private ngZone : NgZone) { }

    mapOptions = {
        layers: [
            tileLayer(OPENSTREETMAP_URL, { maxZoom: 20 })
        ],
        zoom: 12,
        center: MAP_CENTER_QUEBEC
    };

    markers = [];

    ngOnInit() {
        this.stationService.getStations().subscribe(
            stations => stations.map(s => {
                let marker = this.getMarker(s.info.latitude, s.info.longitude, s.aqi.average, s.aqi.color, s.id);
                this.markers.push(marker);
            })
        );
    }

    private getMarker(lat: number, long: number, aqi: number, color: string, stationId: number) : Marker{
        let newMarker = marker([ lat, long ], {
            icon: divIcon({className: 'ship-div-icon',
                iconSize: point(30,35),
                html: getMarkerSvg(color)})
        });

        newMarker.bindTooltip('<span style="font-size: 17px; font-weight: bold">'+aqi+'</span>', {
            offset: point(0,-20),
            direction: 'top',
            permanent: true
        });

        newMarker.addEventListener("click", () => this.onMarkerClicked(stationId));

        return newMarker;
    }

    private onMarkerClicked(stationId:number){
        this.ngZone.run(() => this.router.navigateByUrl("detail-station/" + stationId))
    }
}
