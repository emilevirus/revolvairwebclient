import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {SensorService} from '../../services/sensor-service/sensor.service';
import {Sensor} from '../../models/sensor';
import {ObjectKeyService} from '../../services/object-key-service/object-key.service';

@Component({
  selector: 'detail-sensor',
  templateUrl: './detail-sensor.component.html',
  styleUrls: ['./detail-sensor.component.css'],
})
export class DetailSensorComponent implements OnInit {

  constructor(
    private sensorService : SensorService,
    private route: ActivatedRoute,
    private router : Router
  ) {
    this.route.params.subscribe( params => this.params = params);
  }

  params: Params;
  sensor: Sensor;
  latestValues = [];
  private getObjectKeys = ObjectKeyService.getObjectKeys;

  ngOnInit() {
    this.sensorService.getSensor(this.params["stationId"],
                                  this.params["sensorId"]).subscribe(
      sensor => {
        this.sensor = sensor;
      }
    );

    try {
      this.sensorService.getLatestValues(
        this.params["stationId"],
        this.params["sensorId"]
      ).subscribe(
        latestValues => this.setLatestValues(latestValues)
      );
    }
    catch{
      this.latestValues = null;
    }
  }

  back(){
    this.router.navigate(["detail-station", this.params["stationId"]]);
  }

  private setLatestValues(latestValues){
      for (let key in latestValues) {
        this.latestValues[key] = (latestValues[key]);
      }
    }
}
