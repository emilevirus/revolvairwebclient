import { Injectable } from '@angular/core';

@Injectable()
export class ObjectKeyService {

  constructor() { }

  static getObjectKeys(object: Object): string[]{
    return Object.keys(object);
  }

}
