import {TestBed, inject, tick, fakeAsync} from '@angular/core/testing';

import { ObjectKeyService } from './object-key.service';
import {Observable} from 'rxjs/Observable';

describe('ObjectKeyService', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ObjectKeyService]
    });
  });

  it('should be created', inject([ObjectKeyService], (service: ObjectKeyService) => {
    expect(service).toBeTruthy();
  }));

  describe('getObjectKeys(object: Object)', () => {
    it('should get the station\'s attribute keys', fakeAsync(() => {
      //Arrange
      let object = {};
      const value = "ok";
      object[value] = value;
      let expectedResponse = Object.keys(object);
      tick();

      //Act
      let actual = ObjectKeyService.getObjectKeys(object);
      tick();

      //Assert
      expect(actual).toEqual(expectedResponse)
    }))
  });
});
